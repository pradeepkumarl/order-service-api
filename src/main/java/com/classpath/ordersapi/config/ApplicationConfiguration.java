package com.classpath.ordersapi.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    @ConditionalOnProperty(value = "app.loadUserBean",
                           havingValue = "true",
                           matchIfMissing = true )
    public User userBeanOnPropertyCondition(){
        return new User();
    }

    @Bean
    @ConditionalOnBean(name = "userBeanOnPropertyCondition")
    public User userBeanOnBeanCondition(){
        return new User();
    }
    @Bean
    @ConditionalOnMissingBean(name = "userBeanOnPropertyCondition")
    @ConditionalOnMissingClass(value = "x.y.z")
    public User userBeanOnMissingBeanCondition(){
        return new User();
    }
}

class User {

}
