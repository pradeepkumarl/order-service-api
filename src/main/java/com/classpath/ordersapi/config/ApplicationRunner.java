package com.classpath.ordersapi.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApplicationRunner implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public ApplicationRunner(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Inside the Application runner::");
        System.out.println(" Printing all the registered beans ::");
        String[] beanDefinitionNames = this.applicationContext
                                                .getBeanDefinitionNames();
        List.of(beanDefinitionNames)
                .stream()
                .filter(beanName -> beanName.startsWith("user"))
                .forEach(bean -> System.out.println(bean));
    }
}
