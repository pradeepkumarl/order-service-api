package com.classpath.ordersapi.config;

import com.classpath.ordersapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@Slf4j
class DBHealthCheckEndpoint implements HealthIndicator {

    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        try{
            long noOfOrders = this.orderRepository.count();
            log.info("Number of orders from the table:: {}", noOfOrders);
            return Health.up().withDetail("DB-Service", "DB service is up")
                              .build();
        } catch (Exception exception){
            log.error("Exception connecting to DB :: {} ", exception.getMessage());
            return Health.down().withDetail("DB-Service", "DB service is down")
                    .build();
        }
    }
}