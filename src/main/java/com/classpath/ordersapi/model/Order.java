package com.classpath.ordersapi.model;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;


@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@ToString(exclude = "lineItems")
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Min(value = 2000, message = "Min order value should be 2000 INR")
    @Max(value = 10_000, message = "Max order value can be 10000 INR")
    private double price;

    @PastOrPresent(message = "Order date cannot be in future")
    private LocalDate date;

    @NotEmpty(message = "customer name cannot be empty")
    private String customerName;

    @OneToMany(mappedBy = "order",
               cascade = CascadeType.ALL,
               fetch = FetchType.LAZY,
               orphanRemoval = true
            )
    private Set<LineItem> lineItems;

    //scaffolding code
    // set the bidirectional mapping
    public void addLineItem(LineItem lineItem){
        if(this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }

}
