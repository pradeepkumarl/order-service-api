package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    public Order saveOrder(Order order){
        return this.orderRepository.save(order);
    }

    public Map<String, Object> fetchAllOrders(int page, int size, String direction, String property ){
        log.info("Fetching all the orders from the repository::");
        Sort.Direction sortDirection = direction
                        .equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        PageRequest pageRequest = PageRequest.of(page, size, sortDirection, property);

        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);

        long totalRecords = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();
        int pageNumber = pageResponse.getNumber();
        int numberOfElementsPerPage = pageResponse.getNumberOfElements();
        List<Order> data = pageResponse.getContent();

        Map<String, Object> resultMap = new LinkedHashMap<>();
        resultMap.put("total-records", totalRecords);
        resultMap.put("pages", totalPages);
        resultMap.put("page", pageNumber);
        resultMap.put("records", numberOfElementsPerPage);
        resultMap.put("data", data);
        return resultMap;
    }

    public Order fetchOrderById(long orderId){
        log.info("Fetching the order from the repository with the id :: {}", orderId);
        return this.orderRepository
                .findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("invalid order id"));
    }

    public void deleteOrderById(long orderId) {
        log.info("Deleting the order from the repository with the id :: {}", orderId);
        this.orderRepository.deleteById(orderId);
    }
}
