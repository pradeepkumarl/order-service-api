package com.classpath.ordersapi.event;


import com.classpath.ordersapi.model.LineItem;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import com.github.javafaker.Faker;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Component
@Profile("dev")
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;

    private final Faker faker = new Faker();

    public BootstrapAppData(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        Set<Order> orders = new HashSet<>();
        IntStream.range(0, 100).forEach(index -> {
            Order order = Order.builder()
                        .date(
                                faker.date()
                                        .past(4, TimeUnit.DAYS)
                                        .toInstant()
                                        .atZone(ZoneId.systemDefault()).toLocalDate())
                        .customerName(faker.name().fullName())
                        .price(faker.number().randomDouble(2, 2000, 8_000))
                    .build();

            IntStream.range(0, faker.number().numberBetween(1,4))
                    .forEach(counter -> {
                        LineItem lineItem = LineItem.builder()
                                                        .name(faker.commerce().productName())
                                                        .qty(faker.number().numberBetween(2,10))
                                                        .price(faker.number().randomDouble(2, 5000, 8000))
                                                     .build();
                        order.addLineItem(lineItem);
                    });
            orders.add(order);
        });
    this.orderRepository.saveAll(orders);
    }
}
