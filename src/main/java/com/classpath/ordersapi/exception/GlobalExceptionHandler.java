package com.classpath.ordersapi.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error handleInvalidOrder(IllegalArgumentException exception){
        log.error("Invalid order id passed :: {}", exception.getMessage());
        return new Error(100, exception.getMessage());

    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Set<String> handleInvalidRequest(MethodArgumentNotValidException exception){
        log.error("Constraint violation exception :: {}", exception.getMessage());
        List<ObjectError> errors = exception.getAllErrors();
        Set<String> errorMessages = errors.stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toSet());
        return errorMessages;

    }
}

@AllArgsConstructor
@Getter
class Error {
    private final int code;
    private final String message;
}